%%% Set check to true for validation %%%
check=false;

% Number of simulations
Nens=20;

% Quality of the subspace approximation
percentInfo = 0.95;

% Reading the observations
load('observation-G21.mat');
	  
% Initiatlization	  
Pi = zeros(3,1);

% G�n�ration de la solution � classifier (on sait qu'il est de la cat�gorie
% 1, on doit donc minimiser Pi avec 1 � la fin
[X, ns, nt] = Model(1,1);

for GWi = 1:3
    % Generation of the data set
    Fi = Model(GWi,Nens);
    
    %Computation of the mean and anomalies 
    mFi = mean(Fi,2);
    Zi = Fi-repmat(mFi,1,Nens);
    
    %%%%%%%  Compute the SVD of A    %%%%%%%
    if (check)  
        [U,S,~] = svd(Zi,0);
        D = diag(S);
        if (D(1)==0)
            disp('Alert: the matrix is null')
            return
        end
        converged=1;
        while (D(converged)/D(1)>1-percentInfo) 
            converged=converged+1;
        end
        converged=converged-1;
        U = U(:,1:converged);
        fprintf('dimension of the subspace: %d\n',converged);
    else
        [U,~] = power_method(Zi,percentInfo,1,0.1,maxIter);
        fprintf('dimension du sous espace : %d\n',size(U,2));
        fprintf('erreur de la base : %f\n',norm(U*(U'*Z) - Z,2)/norm(Z,2));
    end
    
    % calcul de l'anomalie
    Zobs = X - mFi;
    
    %%%%% Calcul du pi :
    Pi(GWi) = norm(Zobs - U*(U'*Zobs),2);
end

% Repr�sentation : distance de Zobs � l'espace Fi en fonction de i
figure(1)
bar(Pi)



