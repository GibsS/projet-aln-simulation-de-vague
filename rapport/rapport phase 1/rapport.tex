%----------------------------------------------------------------------------------------
%	REQUIRED PACKAGES
%----------------------------------------------------------------------------------------

\documentclass[
10pt, % Main document font size
a4paper, % Paper type, use 'letterpaper' for US Letter paper
oneside, % One page layout (no page indentation)
%twoside, % Two page layout (page indentation for binding and different headers)
headinclude,footinclude, % Extra spacing for the header and footer
BCOR5mm, % Binding correction
]{scrartcl}

\usepackage[
nochapters, % Turn off chapters since this is an article        
beramono, % Use the Bera Mono font for monospaced text (\texttt)
eulermath,% Use the Euler font for mathematics
pdfspacing, % Makes use of pdftex’ letter spacing capabilities via the microtype package
dottedtoc % Dotted lines leading to the page numbers in the table of contents
]{classicthesis} % The layout is based on the Classic Thesis style


\usepackage[top=3cm, bottom=3cm, left=3cm , right=3cm]{geometry}

\usepackage[french]{babel}
\usepackage{arsclassica} % Modifies the Classic Thesis package

\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs

\usepackage[utf8]{inputenc} % Required for including letters with accents

\usepackage{graphicx} % Required for including images
\graphicspath{{Figures/}} % Set the default folder for images

\usepackage{enumitem} % Required for manipulating the whitespace between and within lists
\usepackage{lipsum} % Used for inserting dummy 'Lorem ipsum' text into the template
\usepackage{subfig} % Required for creating figures with multiple parts (subfigures)
\usepackage{amsmath,amssymb,amsthm} % For including math equations, theorems, symbols, etc
\usepackage{varioref} % More descriptive referencing

\usepackage{algorithm}
\usepackage{algorithmicx} 
\usepackage{algpseudocode}
% mes commandes perso : rajoute section input, output et code dans les algorithmes
\algnewcommand\algorithmicinput{\textbf{INPUT:}}
\algnewcommand\INPUT{\item[\algorithmicinput]}
\algnewcommand\algorithmicoutput{\textbf{OUTPUT:}}
\algnewcommand\OUTPUT{\item[\algorithmicoutput]}
\algnewcommand\test{\textbf{CODE:}}
\algnewcommand\CODE{\item[\test]}

\author{Groupe 21 : Joffrey Guilbon, Antoine Beyet, Emerick Gibson}
\title{Projet d'algèbre linéaire numérique}
\date{}

\begin{document}

\maketitle
\vspace{3cm}
\tableofcontents
\newpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Introduction 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
Ce projet a pour but d'implémenter deux algorithmes autour du modèle de vague de Navier-Stockes.
Nous avons cherché à exploiter la réduction de matrice pour :
\begin{itemize}
	\item Prédire l'évolution de la surface d'une portion d'océan sans utiliser une intégration directe des équations de Navier-Stockes mais en utilisant un certain nombre de solution.
	\item Obtenir les caractéristiques de l'environnement à partir d'une partie des données d'une simulation du modèle et de plusieurs autre simulation dont on connait les caractéristiques.
\end{itemize}

Ce rapport décrit les algorithmes employés et l'influence des paramètres d'entrées de ces derniers sur la complexité temporelle et physique et la précision.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Puissance itérée 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{La méthode de la puissance itérée}

Notre implémentation de la "power method" : 

\begin{algorithm}
\caption{Puissance itérée}\label{euclid}
\begin{algorithmic}[0]
   \INPUT \\
   	\begin{itemize}
   	\item A : matrice dont on cherche la décomposition en valeur singulière partielle
   	\item Percentinfo : pourcentage de précision de la base produite
   	\item max\_iter : le nombre maximum d'itération de l'algorithme 
   	\item epsilon : la précision pour la recherche  des vecteurs propres
   	\end{itemize}
   \OUTPUT \\
   	\begin{itemize}
   	\item U : vecteurs singuliers gauche de A 
   	\item S : valeurs singulières de A
   	\end{itemize}
   \CODE 
   \State Construction d'une base des matrices n*m ([n,m] taille de A)
   \State $converged \gets 0$
   \State $norme\_AA \gets ||A*A'||$
   \While{$(converged = 0 ~ou~ lambda(converged)/lambda(1)>1-Percentinfo) ~et~ niter < max\_iter$}
		\State $Y \gets A*(A'*V)$
		\State $V \gets Gram\_schmidt(Y)$
		\State $H = V'*(A*(A'*V))$
		\State $[X,lambda] \gets décomposition spectrale de H$ 
		\State $V \gets V*X$
		\For {$i = converged+1:m$}
			\If {$||A*(A'*V:,i)) - lambda(i)*V(:,i)||/norme\_AA < epsilon$}
				\State $converged \gets converged + 1$
			\Else
				\State $break$
			\EndIf
			\If {$converged \neq 0 ~et~ lambda(converged)/lambda(1) < 1 - Percentinfo$}
				\State $break$
			\EndIf
		\EndFor
   \EndWhile
\end{algorithmic}
\end{algorithm}

Pour éviter de devoir stocker A*A', nous faisons à chaque fois les calculs progressivement, multipliant A' au vecteur adéquat puis en multipliant le
vecteur obtenu par A.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Reconstruction 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Reconstruction}

Cet algorithme calcule une solution à l'aide de conditions initiales (les données physiques du système à t=0) et un ensemble de solutions au modèle.

\begin{algorithm}
\caption{Reconstruction}\label{euclid}
\begin{algorithmic}[0]
   \INPUT \\
   \begin{itemize}
   \item X : la solution à reconstruire
   \item Nens : le nombre de solution déjà construite
   \item F : une matrice à Nens vecteurs colonnes qui sont des solutions au modèle
   \item Percentinfo : pourcentage de précision de la base produite
   \end{itemize}
   \OUTPUT \\   
   \begin{itemize}
   \item Solution : Vecteur solution du modèle vérifiant les conditions initiales de X
   \end{itemize}
   \CODE 
   \State $muF \gets$ la moyenne des vecteurs de F
   \State $Z \gets vecteurs~colonnes~de~F~auxquels~on~retire~mF$
   \State $X0 \gets X(1 : ns)$ 
   \State $Z0 \gets X0 - muF( 1 : ns)$ 
   \State $[U,S] \gets$ power~method(Z, percent info) 
   \State $U0 \gets U(1 : ns,:)$  
   \State $L \gets Cholesky((U0')*U0)$
   \State $\alpha \gets la~solution~de~L' * x = U0' * Z0$
   \State $\alpha \gets la~solution~de~L * x = \alpha$
   \State $Z\_Solution \gets U * \alpha$
   \State $Solution \gets Z\_Solution + muF$
\end{algorithmic}
\end{algorithm}

Cet algorithme emploie la décomposition de cholesky (implémentation tirée du cours sur les méthodes de résolution directe):

\begin{algorithm}
\caption{Cholesky}\label{euclid}
\begin{algorithmic}[0]
   \INPUT A matrice symétrique définie positive
   \OUTPUT L triangulaire inférieur tel que A = L'*L 
   \CODE
   \State $L \gets triangle~inf\`{e}rieur~de~A$
   \For k=1:n 
    \State $L(k,k) \gets sqrt(L(k,k))$
    \State$L(k+1:n,k) \gets L(k+1:n,k)/L(k,k)$
     \For j=k+1:n 
     	\State $L(j:n,j) = L(j:n,j) - L(j:n,k)*L(j,k)$
     \EndFor
   \EndFor
\end{algorithmic}
\end{algorithm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Classification 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Classification}

Le modèle de Navier-Stockes dépend de plusieurs paramètres. Trois catégories différentes ont été définies pour cette étude regroupant trois différents ensembles de valeurs pour ces paramètres.

Cet algorithme permet, à partir de simulation dans les trois cas différents, d'évaluer dans quel cas se trouve une solution donnée.

\begin{algorithm}
\caption{Classification}\label{euclid}
\begin{algorithmic}[0]
   \INPUT \\
   \begin{itemize}
   \item X : solution à classifier
   \item Nens : le nombre de solution déjà construite pour chaque type d'environnement
   \item Fi : ensemble de solution $i \in [1,3] $
   \end{itemize}  
   \OUTPUT \\
   \begin{itemize}
   \item P : vecteur correspondant aux "distance" de X à chaque sous espace engendré par Fi
   \end{itemize}
   \CODE
   \For i=1:3 
   		\State $mFi \gets moyenne~des~vecteurs~colonnes~de~Fi$
   		\State $Zi \gets vecteurs~colonnes~de~Fi~auxquels~on~retire~mFi$
   		\State $[U,S] \gets$ power~method(Zi, percent info) 
   		\State $Zobs \gets X - mFi$
   		\State $P(i) \gets ||Zobs - U*(U'*Zobs)||$
   \EndFor

\end{algorithmic}
\end{algorithm}

A partir de cet algorithme, on peut choisir le i adéquat : celui qui minimise P.

\section{Influence des paramètres}

Nous cherchons ici à évaluer l'influence du nombre de simulation Nens et du pourcentage Pourcentinfo sur le temps d'exécution et la précision du résultat.

\subsection{Influence sur l'erreur}
Pour ce qui est de l'erreur, nous avons fait tourner les algorithmes plusieurs fois mais n'avons constaté aucun comportement spécifique : l'erreur semble aléatoire.

\subsection{Influence sur le temps de calcul}
\subsubsection{Reconstruction et classification}
La complexité temporelle des algorithmes de reconstruction et de classification tout seul ne dépendent pas de Nens ou de Pourcentinfo mais plus du modèle adopté (nombre de variable spatiale, ..). En effet, dans le cas de reconstruction, la complexité des sous algorithmes (hormis power method, donc cholesky, produit matriciel, résolution de système triangulaire) ne dépendent que de ns, le nombre de variable spatiale. Dans le cas de classification, si on exclut power method, on ne fait qu'un produit matriciel et un calcul de norme, tout deux dépendant de ns, non pas de Nens.

\subsubsection{Puissance itérée}
\underline{Influence de Nens :} Pour la power method, le coût en complexité temporelle le plus cher provient de gram-schmidt quand on cherche à faire la décomposition QR. Cet algorithme rend d'ailleurs notre algorithme bien moins performant que le passage par la méthode 'svd' de matlab. Le coût de gram schmidt est en $\theta(m^2*n)$ où m le nombre de vecteurs colonnes et n le nombre de vecteurs lignes. Or m = Nens dans ce cas, donc la complexité de l'algorithme augmente quadratiquement avec Nens. 

Cette constatation pourrait justifier de faire la première étape de l'itération, le produit par A*A' par V, plusieurs fois : on peut conjecturer que ceci puisse limiter le nombre de fois que l'on est besoin d' exécuter gram schmidt pour converger, le remplaçant par plus d'itération d'un processus moins couteux en complexité.

\underline{Influence de Percentinfo :} Percentinfo n'a que peu d'influence: dans notre cas, quasiment tous les vecteurs convergent en même temps. La modification de Percentinfo ne change que peu le nombre d'itération nécessaire et la taille de la base obtenu par l'algorithme est de faible taille donc a un apport en complexité pour les autres algorithmes négligeable par rapport à ns ou ns*nt.

\section{Résultats}
Nos algorithmes réussissent à avoir des erreurs minimales pour les valeurs des paramètres par défaut. Voici un exemple de résultats pour les deux algorithmes.
\subsection{Reconstruction}
L'algorithme exécuté avec $Nens = 50$ et $Percentinfo = 0.95$. erreur de : 0.0154 par rapport à la solution.
\begin{figure}[h]
\caption{Reconstruction}
\centerline{\includegraphics[width=8cm]{Reconstruction.jpg}}
\end{figure}
\subsection{Classification}
L'algorithme exécuté avec $Nens = 20$ et $Percentinfo = 0.95$ et le paramètre de la simulation était de 1. La figure 2 montre la distance, stocké dans P, de la simulation aux espaces de solution des 3 paramètres de vent. On voit bien que P est minimal pour i = 1.
\begin{figure}[h]
\caption{Classification}
\centerline{\includegraphics[width=7cm]{Classification.jpg}}
\end{figure}
\end{document}