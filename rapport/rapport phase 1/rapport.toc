\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}La m\IeC {\'e}thode de la puissance it\IeC {\'e}r\IeC {\'e}e}{2}{section.2}
\contentsline {section}{\numberline {3}Reconstruction}{3}{section.3}
\contentsline {section}{\numberline {4}Classification}{3}{section.4}
\contentsline {section}{\numberline {5}Influence des param\IeC {\`e}tres}{4}{section.5}
\contentsline {subsection}{\numberline {5.1}Influence sur l'erreur}{4}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Influence sur le temps de calcul}{4}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Reconstruction et classification}{4}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Puissance it\IeC {\'e}r\IeC {\'e}e}{4}{subsubsection.5.2.2}
\contentsline {section}{\numberline {6}R\IeC {\'e}sultats}{5}{section.6}
\contentsline {subsection}{\numberline {6.1}Reconstruction}{5}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Classification}{5}{subsection.6.2}
