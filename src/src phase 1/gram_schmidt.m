function Q = gram_schmidt(A)
   
[n, m] = size(A);
Q=A;
for i=1:m
    y = A(:,i);
    for j=1:i-1
        y = y - (Q(:,j)'*y)*Q(:,j);
    end
    Q(:,i) = y/norm(y);
end

