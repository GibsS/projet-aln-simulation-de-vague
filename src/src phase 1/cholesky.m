function L = cholesky(A)

    L = zeros(size(A));
    n = size(A,1);
    for k = 1:n
        for j = 1:k
            L(k,j) = A(k,j);
        end
    end

    for k = 1:n
        L(k,k) = sqrt(L(k,k));
        L(k+1:n,k) = L(k+1:n,k)/L(k,k);
        for j = k+1:n
            L(j:n,j) = L(j:n,j) - L(j:n,k)*L(j,k);
        end 
    end
