function [V,lambda] = power_method(A,percentInfo,p, epsilon, max_iter)
    % Cette impl�mentation est une version de la puissance it�r�e
    % s'appliquant � A*A'
    niter = 0;
    [n,m] = size(A);
    norme_A = norm(A,2);
    V = zeros(n,m);
    lambda = zeros(m);
    converged = 0; 
    
    % Cr�ation d'une base des matrices r�elles de taille n*m
    for i = 1:m
        V(i,i) = 1;
    end
    
    while ((converged == 0 || lambda(converged)/lambda(1)>1-percentInfo) && niter < max_iter) 
        % notre solution pour �viter le calcul direct est de faire le
        % produit progressivement : d'abord A'*V, puis A*(A'*V), �vitant au
        % moins l'allocation monstrueuse de m�moire pour A*A'
        for i=1:p
            Y = A'*V;
            Y = A*Y;
        end
        % orthonalization de Y
        V = gram_schmidt(Y);
        
        % Calcul des vecteurs propres et valeurs propres du quotient de
        % Rayleigh
        H = V'*(A*(A'*V));
        [X,l] = eig(H);
        % S'assurer que les valeurs propres sont tri� dans l'ordre
        % d�croissant
        [lambda,I] = sort(diag(l),'descend');
        X = X(:,I);
        
        % Evaluation des vecteurs propres associ�s � A*A' � partir de ce de
        % H
        V = V*X;
        for i = converged+1:m
            if (norm(A*(A'*V(:,i)) - lambda(i)*V(:,i),2)/(norme_A*norme_A) < epsilon)
                converged = converged + 1;
            else
                break;
            end
            % si on a d�j� atteint le nombre de vecteur requis, on peut
            % s'arr�ter imm�diatement et ne pas regarder les vecteurs
            % suivant.
            if(converged ~= 0 && lambda(converged)/lambda(1)<1-percentInfo)
                break;
            end
        end
        niter = niter + 1;
    end
    
    V = V(:,1:converged);
    lambda = lambda(1:converged);